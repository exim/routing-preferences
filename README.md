# Routing Preferences

In this project we compare explicit (self-reported) preferences for speed, low traffic, and low complexity of routes to participants implicit preferences for these factors (inferred from their choice of routes). 
We use online experiments to test our assumptions in this project.

The association of explicit and implicit preferences is expected to be positive (e.g. a person who self-reports to prefer fast routes will also choose the fastest routes when presented with different options on a map). 

We further explore factors which could influence the association between explicit and implicit routing preferences. For example, participants to whom their explicitly stated preferences are very important should be more likely to choose routes that reflect these preferences. 
As an experimental factor, we use maps in which the cost factors (speed, low traffic, low complexity) are labelled versus maps without labels. Labelling the map might help participants to align their explicit preferences with their route choices (implicit preferences).


